!include "MUI.nsh"
!include "./FileAssociation.nsh"

Var MUI_TEMP
Var STARTMENU_FOLDER

;Name and file
Name "@PACKAGE_NAME@"
OutFile "@PACKAGE_NAME@-@PACKAGE_VERSION@.exe"

;Default installation folder
InstallDir "$PROGRAMFILES\@PACKAGE_NAME@"

;Get installation folder from registry if available
InstallDirRegKey HKCU "Software\@PACKAGE_NAME@" ""

!define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\@PACKAGE_NAME@" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"

  !insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "@PACKAGE_NAME@" SecMain

  SetOutPath "$INSTDIR"
  File src/biloba.exe
  ;FIXME Hardcoded path to SDL dlls.
  File /opt/SDL-1.2.13/runtime/*dll

  SetOutPath "$INSTDIR\res\common"
  File res/common/*.png
  SetOutPath "$INSTDIR\res\en"
  File res/en/*.png
  SetOutPath "$INSTDIR\res\es"
  File res/es/*png
  SetOutPath "$INSTDIR\res\fr"
  File res/fr/*png
  SetOutPath "$INSTDIR\res\snd"
  File res/snd/*wav
  
  ;Store installation folder
  WriteRegStr HKCU "Software\@PACKAGE_NAME@" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
  ;Create shortcuts
  CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\@PACKAGE_NAME@.lnk" "$INSTDIR\Biloba.exe"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\@PACKAGE_NAME@" "DisplayName" \
    "@PACKAGE_NAME@"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\@PACKAGE_NAME@" "UninstallString" \
    "$INSTDIR\Uninstall.exe"
  
  ${registerExtension} "$INSTDIR\@PACKAGE_NAME@.exe" ".blb" "Biloba Game"

  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecMain ${LANG_ENGLISH} "@PACKAGE_NAME@"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecMain} $(DESC_SecMain)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  Delete "$INSTDIR\res\common\*"
  RMDir "$INSTDIR\res\common"
  Delete "$INSTDIR\res\en\*"
  RMDir "$INSTDIR\res\en"
  Delete "$INSTDIR\res\es\*"
  RMDir "$INSTDIR\res\es"
  Delete "$INSTDIR\res\fr\*"
  RMDir "$INSTDIR\res\fr"
  Delete "$INSTDIR\res\snd\*"
  RMDir "$INSTDIR\res\snd"
  RMDir "$INSTDIR\res"
  Delete "$INSTDIR\*"

  RMDir "$INSTDIR"

  !insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
    
  Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"
  Delete "$SMPROGRAMS\$MUI_TEMP\@PACKAGE_NAME@.lnk"
  
  ;Delete empty start menu parent diretories
  StrCpy $MUI_TEMP "$SMPROGRAMS\$MUI_TEMP"
 
  startMenuDeleteLoop:
	ClearErrors
    RMDir $MUI_TEMP
    GetFullPathName $MUI_TEMP "$MUI_TEMP\.."
    
    IfErrors startMenuDeleteLoopDone
  
    StrCmp $MUI_TEMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop
  startMenuDeleteLoopDone:

  DeleteRegKey /ifempty HKCU "Software\@PACKAGE_NAME@"
  ${unregisterExtension} ".blb" "@PACKAGE_NAME@ Game"

SectionEnd
