/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file handles pawn and tile selection via
 * the local method.
 */

#include <SDL.h>
#include "utils.h"
#include "local_input.h"
#include "tile.h"
#include "net.h"

/**
 * Select a tile using the local input (mouse)
 *
 * @return the tile selected, or NULL if the selection was
 * outside the board's tiles.
 */
Tile *local_select_tile(void)
{
	Tile *result = NULL;
	SDL_Event event;
	int x, y;

#ifdef DEBUG
	printf("local_input: waiting for event\n");
#endif
	event = get_sdl_event(SDL_MOUSEBUTTONUP);
	if (event.type == SDL_USEREVENT_QUIT || event.type == SDL_QUIT)
		return NULL; /* exit waiting */

	SDL_GetMouseState (&x, &y);

#ifdef DEBUG
	printf("local_input: got event (%d, %d)\n", x, y);
#endif
	
	result = tile_get_by_coords(x, y);
	return result;
}
