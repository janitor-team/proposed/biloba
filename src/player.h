/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "pawn.h"
#include "tile.h"
#include "llist.h"

typedef enum {
	INPUT_LOCAL,
	INPUT_NETWORK,
	INPUT_AI,
	INPUT_REPLAY,
	NUM_INPUT_SYSTEMS
} InputSystemMethod;

static inline char *input_system_names(InputSystemMethod i) {
	char *name[3] = {
			"LOCAL",
			"NETWORK",
			"AI" };
	if (i < 0 || i >= NUM_INPUT_SYSTEMS)
		return "WRONG TYPE";

	return name[i];
};


        
typedef struct _Player {
	PawnColor color;
	InputSystemMethod method;
	const char *name;
} Player;

Player *player_get(PawnColor color, int reinit, InputSystemMethod method);
LList *player_can_eat_soon(Player *player, int now_too, LList *allowed_pawns);
LList *player_can_be_eaten(Player *player, int now_too, LList *allowed_pawns);

Tile *player_select_tile(Player *player, LList *allowed);

void player_set_name(Player *player, const char *name);

#endif
