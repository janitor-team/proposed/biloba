/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the basic network I/O code.
 */

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <errno.h>
#ifndef __MINGW32__
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#else
#include <winsock2.h>
#endif

#include "netops.h"

#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL 0
#endif

int get_net_error(void)
{
#ifndef __MINGW32__
	return -errno;
#else
	return -WSAGetLastError();
#endif
}

const char *net_err_str(int err)
{
	return strerror(-err);
}

int send_msg(int fd, const void* buf, unsigned char len)
{
	int rem = len;
	int sent = 0;
	
	sent = (int)send(fd, (char *)&len, 1, MSG_NOSIGNAL);
	if (sent < 1) {
		return get_net_error();
	}
	
	do {
		sent = (int)send(fd, buf, (size_t)rem, MSG_NOSIGNAL);
		if (sent < 0) {
			rem = 0;
			return get_net_error();
		} else {
			rem -= sent;
			buf += sent;
		}
	} while (rem > 0);
	return len;
}

int send_int(int fd, int32_t n)
{
	int32_t x = htonl(n);

	return send_msg(fd, &x, sizeof(x));
}

int send_char(int fd, char c)
{
	return send_msg(fd, &c, sizeof(c));
}

int send_str(int fd, const char *str)
{
	int len = strlen(str) + 1;

	if (len > 255)
		return -E2BIG;

	return send_msg(fd, str, strlen(str) + 1);
}

int read_msg(int fd, void *buf, unsigned char avail, unsigned char *len)
{
	int readlen = 0;
	unsigned char gotlen = 0;
	int rem = 0;
	
	memset (buf, 0, (size_t)avail);
	readlen = recv(fd, (char *)&gotlen, 1, MSG_NOSIGNAL);

	if (readlen == 0) {
		*len = 0;
		return -EIO;
	} else if (readlen < 0) {
		*len = 0;
		return get_net_error();
	}
	
	rem = gotlen;
	if (rem > 255 || rem > avail) {
		*len = 0;
		return -E2BIG;
	}
	
	do {
		readlen = recv(fd, buf, rem, MSG_NOSIGNAL);
                if (readlen == 0) {
			*len = 0;
			return -EIO;
                } else if (readlen < 0) {
			*len = 0;
			return get_net_error();
		} else {
			rem -= readlen;
			buf += readlen;
		}
	} while (rem > 0);
	
	*len = gotlen;
	
	return gotlen;
}

int read_int(int fd, int32_t *n)
{
	int r;
	unsigned char len;
	int32_t x;

	r = read_msg(fd, &x, sizeof(x), &len);
	if (r <= 0)
		return r;

	*n = ntohl(x);

	return len;
}

int read_char(int fd, char *c)
{
	unsigned char len;

	return read_msg(fd, c, 1, &len);
}

int read_str(int fd, char *str, int max_len)
{
	unsigned char len;
	int r;

	r = read_msg(fd, str, max_len, &len);
	
	str[max_len - 1] = '\0';

	return r;
}

int sock_select(int sock, int timeout_msec)
{
	fd_set set, except_set;
	struct timeval tm_timeout;
	tm_timeout.tv_sec = timeout_msec / 1000;
	tm_timeout.tv_usec = (timeout_msec % 1000) * 1000;

	FD_ZERO(&set);
	FD_SET(sock, &set);
	FD_ZERO(&except_set);
	FD_SET(sock, &except_set);

	if (select(sock + 1, &set, NULL, &except_set, &tm_timeout) < 0) {
		return -1;
	}
	return (FD_ISSET(sock, &set) || FD_ISSET(sock, &except_set)) ? 0 : -1;
}

void sock_close(int sock)
{
	shutdown(sock, SHUT_RDWR);
	close(sock);
}

void get_bcast_msg(char *msg, size_t len, int magic_number)
{
	memset(msg, 0, len);
	snprintf(msg, len, "Biloba%32d", magic_number);
}


const char *get_hostname(struct sockaddr_in *addr)
{
#ifndef WIN32
	static char host[128];
	int err = getnameinfo((struct sockaddr *)addr, sizeof(struct sockaddr_in),
			host, sizeof(host), NULL, 0, NI_NAMEREQD | NI_NOFQDN);

	if (err != 0 || *host == '\0')
		return NULL;
	
	return host;
#else
	static char host[128];
	struct hostent *hostent = gethostbyaddr((char *)&addr->sin_addr.s_addr,
					sizeof(addr->sin_addr.s_addr),
					AF_INET);

	if (hostent == NULL || hostent->h_name == NULL
	 || *(hostent->h_name) == '\0')
		return NULL;

	strncpy(host, hostent->h_name, sizeof(host));
	
	if (strchr(host, '.'))
		*strchr(host, '.') = '\0';

	return host;
#endif
}	
