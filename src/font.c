/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains all the font management code.
 */

#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>

#include <ctype.h>
#include <string.h>

#include "utils.h"
#include "font.h"

static SDL_Surface *font_pic = NULL;
static SDL_Surface *cursor_pic = NULL;

/**
 * Load the font from disk
 */
static void load_font(void)
{
	SDL_Surface *tmp = NULL;
	assert(font_pic == NULL);
	
	tmp = biloba_load_image("font.png");
	font_pic = SDL_DisplayFormatAlpha(tmp);
	SDL_FreeSurface(tmp);
	cursor_pic = biloba_load_image("cursor.png");

	assert(font_pic != NULL);	
	assert(cursor_pic != NULL);	
}

/**
 * Draw a message on screen and update the screen
 *
 * @param[in] message	The text to draw
 * @param[in] x		The X coordinate (in pixels)
 * @param[in] y		The Y coordinate (in pixels)
 * @param[in] width	Available width (-1 means no constraint)
 * @param[in] cursor	Whether to draw a cursor after the text
 */
void draw_message(const char *message, int x, int y, int width, int cursor)
{
	draw_message_with_update(message, x, y, width, cursor, TRUE);
}

/**
 * Draw a message on screen
 *
 * @param[in] message	The text to draw
 * @param[in] x		The X coordinate (in pixels)
 * @param[in] y		The Y coordinate (in pixels)
 * @param[in] width	Available width (-1 means no constraint)
 * @param[in] cursor	Whether to draw a cursor after the text
 * @param[in] update	Whether to update the screen after drawing
 */
void draw_message_with_update(const char *message, int x, int y, int width,
			      int cursor, int update)
{
	int i = 0;
	int cur_x = x;
	static SDL_Rect *dst_rect = NULL;
	static SDL_Rect *src_rect = NULL;
	int writable;
	int pix_len;
	int first_char, last_char;

	if (dst_rect == NULL)
		dst_rect = malloc(sizeof(SDL_Rect));
	if (src_rect == NULL)
		src_rect = malloc(sizeof(SDL_Rect));
	
	src_rect->x = CHARWIDTH; /* skip space */
	src_rect->y = 0;
	src_rect->w = CHARWIDTH;
	src_rect->h = LINEHEIGHT;

	if (font_pic == NULL) {
		load_font();
	}
	
	if (width > -1)
		writable = width / CHARWIDTH;
	else
		writable = strlen(message);

	pix_len = strlen(message) * CHARWIDTH;

	if (!cursor) {
		/* display first chars */
		first_char = 0;
		last_char = min(writable, strlen(message));
	} else {
		/* one place for the cursor */
		writable--;
		/* display last chars */
		last_char = strlen(message);
		first_char = last_char - writable;
		if (first_char < 0)
			first_char = 0;
	}

	for (i = first_char; i < last_char; i ++) {
		if (message[i] == '\n') {
			cur_x = x;
			y += LINEHEIGHT;
			if (!cursor)
				last_char += i;
			continue;
		}

		dst_rect->x = cur_x;
		dst_rect->y = y + LINEYOFFSET;
		
		src_rect->x = (unsigned char)message[i] * CHARWIDTH;

#ifdef DEBUG
		printf("putting char %c (%d,%d) at (%d,%d)\n",
				message[i], src_rect->x, src_rect->y,
				dst_rect->x, dst_rect->y);
#endif
		SDL_BlitSurface(font_pic, src_rect, screen, dst_rect);	
		cur_x += CHARWIDTH;
	}
	if (cursor) {
		put_image(cursor_pic, cur_x, y + LINEYOFFSET);
	}
#ifdef DEBUG
	printf("updating %d,%d pixels from %d,%d\n",
			cur_x - x, LINEHEIGHT, x, y + LINEYOFFSET);
#endif
	if (update)
		SDL_UpdateRect(screen, x, y + LINEYOFFSET, cur_x - x + (cursor?CHARWIDTH:0), LINEHEIGHT);
}

/**
 * Clear text
 *
 * @param[in] length	The number of characters to clear
 * @param[in] x		The X coordinate (in pixels)
 * @param[in] y		The Y coordinate (in pixels)
 */

void clear_text(int length, int x, int y)
{
	static SDL_Rect *dst_rect = NULL;
	if (dst_rect == NULL)
		dst_rect = malloc(sizeof(SDL_Rect));
	
	dst_rect->x = x;
	dst_rect->y = y + LINEYOFFSET;
	dst_rect->w = length > -1 ? CHARWIDTH * length : XS - dst_rect->x;
	dst_rect->h = LINEHEIGHT;

	SDL_FillRect(screen, dst_rect, 0x00000000);
	SDL_UpdateRect(screen, dst_rect->x, dst_rect->y, dst_rect->w, dst_rect->h);
}
