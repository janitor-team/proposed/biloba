/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file only contains the game freer used by options.c
 * and server.c.
 */

#include <stdlib.h>
#include "game.h"

Game *init_game()
{
	int i = 0;
	Game *new_game = malloc(sizeof(Game));

	if (new_game == NULL)
		return NULL;

	memset(new_game, 0, sizeof(Game));

	new_game->moves = NULL;
	new_game->move_id = -1;

	new_game->messages = NULL;
	new_game->message_id = -1;

	for (i = 0; i < 4; i++) {
		new_game->player_name[i] = NULL;
		new_game->players_move_id[i] = -1;
		new_game->players_msg_id[i] = -1;
	}
		
	return new_game;
}

/**
 * Free a game struct
 * @param game		The game to free
 */
void free_game(Game *game)
{
	int i = 0;

	if (!game)
		return;

	free(game->name);
	free(game->moves);
	free(game->messages);

	for (i = 0; i < 4; i++)
		free(game->player_name[i]);

	SDL_DestroyCond(game->cond_wait);
	SDL_DestroyCond(game->cond_move);
	SDL_DestroyCond(game->cond_quit);

	free(game);
}

void game_push_move(Game *game, int player, int x, int y)
{
	int new_id;
	
	if (!game)
		return;

	new_id = game->move_id + 1;

	game->moves = realloc(game->moves, (new_id + 1) * sizeof(Move));
	game->moves[new_id].player    = player;
	game->moves[new_id].x	      = x;
	game->moves[new_id].y	      = y;
	game->move_id		      = new_id;
	game->players_move_id[player] = game->move_id;

	if (game->cond_move)
		SDL_CondBroadcast(game->cond_move);
}

void game_push_message(Game *game, int player, const char *msg)
{
	int new_id;
	
	if (!game)
		return;

	new_id = game->message_id + 1;

	game->messages = realloc(game->messages, (new_id + 1) * sizeof(Message));
	game->messages[new_id].player = player;
	strncpy(game->messages[new_id].message, msg,
		sizeof(game->messages[new_id].message));

	game->message_id	      = new_id;
}
