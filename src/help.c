/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the help code. Quite simple,
 * just a slideshow.
 */

#include <SDL.h>
#include <SDL_image.h>
#include "utils.h"
#include "help.h"

#define N_PAGES 6

static SDL_Surface *help_img[N_PAGES] = {NULL};

/**
 * Load all help images.
 */
static void help_load_images(void)
{
	int i = 0;
	char tmp[255];
	
	for (i = 0; i < N_PAGES; i++) {
#ifndef MAEMO
		sprintf(tmp, "aide%d.png", (i+1));
#else
		sprintf(tmp, "aide%d-maemo.png", (i+1));
#endif
		help_img[i] = biloba_load_image(tmp);
		assert(help_img[i]);
	}
}

/**
 * Start the help slideshow, loading the images first if necessary.
 */
void help_start(void)
{
	int cur_page = 0;
	if (!help_img[0])
		help_load_images();
	
	for (cur_page = 0; cur_page < N_PAGES; cur_page++) {
		put_image(help_img[cur_page], 0, 0);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
		
		get_sdl_event(SDL_MOUSEBUTTONUP);
	}
}
