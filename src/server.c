/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

/**
 * Biloba - Q1 2005
 * Game by Guillaume Demougeot <dmgt@wanadoo.fr>
 * Code by Colin Leroy <colin@colino.net>
 *
 * This file contains the network server code.
 */

#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 

#ifndef __MINGW32__
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#else
#include <winsock2.h>
#endif

#include <sys/time.h>
#include <time.h>

#include <SDL.h>
#include <SDL_thread.h>
#include <SDL_mutex.h>

#include "server.h"
#include "utils.h"
#include "netops.h"
#include "llist.h"
#include "game.h"
#include "player.h"
#include "pawn.h"
#include "replay.h"

static FILE *logfp = NULL;
static const char *dump_dir = NULL;

static unsigned int cur_id = 0;
SDL_mutex *gamelist_mutex = NULL;
static LList *gamelist = NULL;

#ifndef __MINGW32__
static int optOn = 1;
#else
static const char optOn = 1;
#endif

typedef enum {
	CONN_NEW,
	CONN_SETUP_NEW,
	CONN_SETUP_LISTGAME,
	CONN_SETUP_PRELJOIN,
	CONN_SETUP_TALK_MODE,
	CONN_SETUP_JOIN,
	CONN_WAIT_PLAYERS,
	CONN_START_PLAY,
	CONN_PLAY,
	CONN_PLAY_SEND_MOVE,
	CONN_PLAY_SEND_QUIT,
	CONN_PLAY_RECEIVE,
	CONN_WAIT_MESSAGE,
	CONN_SEND_MESSAGE,
	CONN_RECV_MESSAGE,
	CONN_KILL,
	CONN_EXIT,
	NUM_CONN_STATES
} ConnState;

typedef struct _ConnData {
	int fd;
	SDL_Thread *thread;
	char *addr;
	ConnState state;

	Game *game;
	int previously_reserved;
	int player_num;
	int am_initiator;
	int last_sent_join;
	int talk_mode;
} ConnData;

static ConnData *conn_data_init(int fd, const char *addr)
{
	ConnData *cd = malloc(sizeof(ConnData));

	if (cd == NULL)
		return NULL;

	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR,
		&optOn, sizeof(optOn));

	cd->fd = fd;
	cd->thread = NULL;
	cd->addr = strdup(addr);
	cd->state = CONN_NEW;
	cd->game = NULL;
	cd->previously_reserved = -1;
	cd->player_num = -1;
	cd->am_initiator = 0;
	cd->last_sent_join = -1;
	cd->talk_mode = 0;

	return cd;
}

static char state_buf[32];
static char *state_to_str(ConnState state)
{
	switch(state) {
	case CONN_NEW:			return "CONN_NEW";
	case CONN_SETUP_NEW:		return "CONN_SETUP_NEW";
	case CONN_SETUP_LISTGAME:	return "CONN_SETUP_LISTGAME";
	case CONN_SETUP_PRELJOIN:	return "CONN_SETUP_PRELJOIN";
	case CONN_SETUP_JOIN:		return "CONN_SETUP_JOIN";
	case CONN_SETUP_TALK_MODE:	return "CONN_SETUP_TALK_MODE";
	case CONN_WAIT_PLAYERS:		return "CONN_WAIT_PLAYERS";
	case CONN_START_PLAY:		return "CONN_START_PLAY";
	case CONN_PLAY:			return "CONN_PLAY";
	case CONN_PLAY_SEND_MOVE:	return "CONN_PLAY_SEND_MOVE";
	case CONN_PLAY_SEND_QUIT:	return "CONN_PLAY_SEND_QUIT";
	case CONN_PLAY_RECEIVE:		return "CONN_PLAY_RECEIVE";
	case CONN_WAIT_MESSAGE:		return "CONN_WAIT_MESSAGE";
	case CONN_SEND_MESSAGE:		return "CONN_SEND_MESSAGE";
	case CONN_RECV_MESSAGE:		return "CONN_RECV_MESSAGE";
	case CONN_KILL:			return "CONN_KILL";
	case CONN_EXIT:			return "CONN_EXIT";
	default:			
		snprintf(state_buf, sizeof(state_buf), "%d", state);
		return state_buf;
	}
}

static void do_log(ConnData *cd, const char *format, ...)
{
	va_list args;
	struct timeval tv;
	time_t t;
#ifndef __MINGW32__
	struct tm tm;
#endif
	gettimeofday(&tv, NULL);
	t = time(NULL);
#ifndef __MINGW32__
	localtime_r(&t, &tm);

	fprintf(logfp, "%02d/%02d/%4d %02d:%02d:%02d.%04d [%s(%d)] - ", 
		    tm.tm_mday,
		    tm.tm_mon +1,
		    tm.tm_year + 1900,
		    tm.tm_hour,
		    tm.tm_min,
		    tm.tm_sec,
		    (int)(tv.tv_usec / 1000),
		    cd ? cd->addr : "Server ",
		    cd ? cd->fd : 0);
#else
	fprintf(logfp, "%s [%s(%d)] - ", 
		    asctime(localtime(&t)),
		    cd ? cd->addr : "Server ",
		    cd ? cd->fd : 0);
#endif

	va_start(args, format);
	vfprintf(logfp, format, args);
	va_end(args);
	
	fflush(logfp);
}

static char *sanitize(char *str)
{
	while (strstr(str, DIR_SEP))
		*strstr(str, DIR_SEP) = '_';

	return str;
}

static void dump_game(Game *game)
{
	char dump_file[512];

	if (snprintf(dump_file, sizeof(dump_file), "%s%s%d-%s.blb",
			dump_dir, DIR_SEP, game->id,
			sanitize(game->name)) >= sizeof(dump_file)) {
		do_log(NULL, "Can't dump game %d (%s): File name too long\n",
			game->id, game->name);
		return;
	}

	if (replay_dump_game(game, dump_file) < 0)
		do_log(NULL, "Can't dump game %d (%s) to %s: %s\n",
			game->id, game->name, dump_file, strerror(errno));
}

/* must be called with gamelist_mutex held */
static LList *get_games(int num_players, int max)
{
	LList *cur = gamelist;
	LList *results = NULL;
	int i = 0;
	
	while (cur) {
		Game *game = (Game *)cur->data;
		if (game->num_players == num_players &&
		    game->first_avail_spot > 0 &&
		    game->started == 0 &&
		    game->killed == 0) {
			results = llist_append(results, game);
			i++;
		}
		if (i >= max) 
			break;
		cur = cur->next;
	}
	
	return results;
}

static Game *get_game(int id)
{
	LList *cur = gamelist;
	
	while (cur) {
		Game *game = (Game *)cur->data;
		if (game->id == id)
			return game;

		cur = cur->next;
	}
	return NULL;
}

static int get_nb_missing_players(Game *game)
{
	int i = 0;
	int r = 0;
	if (game == NULL)
		return 0;
	for (i = 0; i < game->num_players; i++) {
		if (game->player_name[i] == NULL)
			r++;
	}
	return r;
}

static void set_timeouts(ConnData *cd, int seconds)
{
#ifndef __MINGW32__
	struct timeval timeo;
	timeo.tv_sec = seconds;
	timeo.tv_usec = 0;
	if (setsockopt(cd->fd, SOL_SOCKET, SO_RCVTIMEO, &timeo, sizeof(timeo)) < 0)
		perror("setsockopt");
	if (setsockopt(cd->fd, SOL_SOCKET, SO_SNDTIMEO, &timeo, sizeof(timeo)) < 0)
		perror("setsockopt");
#else
	DWORD timeo = seconds * 1000;
	if (setsockopt(cd->fd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&timeo, sizeof(timeo)) < 0)
		perror("setsockopt");
	if (setsockopt(cd->fd, SOL_SOCKET, SO_SNDTIMEO, (const char *)&timeo, sizeof(timeo)) < 0)
		perror("setsockopt");
#endif
}

static int server_setup_game(ConnData *cd, Game **new_game)
{
	char *p_name;
	char buf[256];
	unsigned char len, i, p;
	int numplayers = 0;

	*new_game = init_game();

	if (send_str(cd->fd, "OK") < 0)
		goto error;

	/* read game name */
	if (read_str(cd->fd, buf, 255) < 0) 
		goto error;

	do_log(cd, " > game name '%s'\n", buf);

	(*new_game)->name = strdup(buf);

	cur_id++;
	if (send_int(cd->fd, cur_id) < 0)
		goto error;

	do_log(cd, " < game ID %d\n", cur_id);

	(*new_game)->id = cur_id;
	for (i = 0; i < 4; i++) {
		(*new_game)->player_name[i] = NULL;
	}
	/* read player info */
	if (read_msg(cd->fd, buf, 255, &len) < 0)
		goto error;

	i = 0;
	numplayers = (int) buf[i]; i++;

	do_log(cd, " > %d players\n", numplayers);
	if (numplayers < 1 || numplayers > 4) {
		send_str(cd->fd, "ERROR");
		do_log(cd, "   wrong number of players\n");
		goto error;
	}

	(*new_game)->num_players = numplayers;
	(*new_game)->first_avail_spot = -1;


	for (p = 0; p < numplayers; p++) {
		(*new_game)->player_type[p] = (InputSystemMethod)buf[i];
		
		if ((*new_game)->player_type[p] < 0 ||
		    (*new_game)->player_type[p] >= NUM_INPUT_SYSTEMS) {
			send_str(cd->fd, "ERROR");
			do_log(cd, "   wrong player type %d for player %d\n",
				(*new_game)->player_type[p], p);
			goto error;
		}

		i++; p_name = buf+i;
		do_log(cd, " > player %d (%s) is named %s\n", 
			p,
			input_system_names((*new_game)->player_type[p]),
			p_name);

		(*new_game)->player_name[p] = strdup(p_name);
		i+=strlen(p_name)+1;
	}

	(*new_game)->num_remote = 0;
	(*new_game)->first_avail_spot = -1;
	for (i = 0; i < numplayers; i++) {
		if ((*new_game)->player_type[i] == INPUT_NETWORK) {
			free((*new_game)->player_name[i]);
			(*new_game)->player_name[i] = NULL;
			(*new_game)->num_remote++;
			do_log(cd, "   cleared player %d's name (net player)\n", i);
			if ((*new_game)->first_avail_spot == -1) {
				(*new_game)->first_avail_spot = i + 1;
					do_log(cd, "   first spot for another player is %d\n",
						(*new_game)->first_avail_spot);
			}
		}
	}

	if ((*new_game)->first_avail_spot == -1) {

		do_log(cd, "   ERROR: could not get a spot for another player\n");
		send_str(cd->fd, "ERROR");
		goto error;
	}

	if (send_str(cd->fd, "READY") < 0)
		goto error;

	do_log(cd, "   < game %s (%d) fully set up\n", (*new_game)->name,
		(*new_game)->id);

	(*new_game)->cond_wait = SDL_CreateCond();
	(*new_game)->cond_move = SDL_CreateCond();
	(*new_game)->cond_quit = SDL_CreateCond();

	SDL_LockMutex (gamelist_mutex);
	gamelist = llist_append(gamelist, (*new_game));
	SDL_UnlockMutex (gamelist_mutex);
	return 0;
error:
	free_game(*new_game);
	*new_game = NULL;
	return -1;
}

static int server_get_requested_num_players(ConnData *cd)
{
	int nump;

	if (send_str(cd->fd, "NUMP") < 0)
		return -1;

	if (read_int(cd->fd, &nump) < 0)
		return -1;

	return nump;
}

static int server_send_games(ConnData *cd, int nump)
{
	LList *avail = NULL, *cur;
	int err;

	SDL_LockMutex (gamelist_mutex);

	avail = get_games(nump, 6);

	if (send_int(cd->fd, llist_length(avail)) < 0) {
		err = -1;
		goto error;
	}

	cur = avail;

	do_log(cd, " < sending %d games available\n", llist_length(avail));

	/* send up to 6 games: id, name */
	while (cur) {
		Game *game = cur->data;

		if (send_int(cd->fd, game->id) < 0) {
			err = -1;
			goto error;
		}


		if (send_str(cd->fd, game->name) < 0) {
			err = -1;
			goto error;
		}

		do_log(cd, "   sent game ID %d, name '%s'\n", game->id, game->name);

		cur = cur->next;
	}
	err = 0;
error:
	SDL_UnlockMutex (gamelist_mutex);
	llist_free(avail);
	return err;
}

static int server_send_player_names(ConnData *cd, Game *game)
{
	int j;
	for (j = 0; j < game->num_players; j++) {
		const char *name = game->player_name[j] ? game->player_name[j] : "";
		if (send_str(cd->fd, name) < 0)
			return -1;

		do_log(cd, " > sent player %d name '%s'\n", j,
			game->player_name[j] ? game->player_name[j] : "");
	}
	return 0;
}

static int server_get_first_spot(ConnData *cd, Game *game)
{
	if (send_int(cd->fd, game->first_avail_spot) < 0)
		return -1;

	do_log(cd, " < sent %d (first available spot)\n", game->first_avail_spot);

	game->first_avail_spot++;

	/* No more free spot, game would be able to begin if player joins */
	if (game->first_avail_spot > game->num_players)
		game->first_avail_spot = -1;
	
	return 0;
}

static void server_put_first_spot_back(ConnData *cd, Game *game)
{
	if (game != NULL) {
		/* put back in pool */
		game->first_avail_spot--;
		if (game->first_avail_spot < 0)
			game->first_avail_spot = game->num_players;
		do_log(cd, "   un-reserved spot on game %d (%s)\n", game->id, game->name);
	}
}			
static Game *server_do_reserve_game(ConnData *cd, int game_id)
{
	Game *game;

	if (cd->previously_reserved > 0) {
		do_log(cd, "   de-reserving spot in previously reserved game %d\n",
			cd->previously_reserved);
		server_put_first_spot_back(cd, get_game(cd->previously_reserved));
		cd->previously_reserved = -1;
	}

	game = get_game(game_id);
	if (game == NULL) {
		do_log(cd, "   game %d does not exist\n", game_id);
		send_str(cd->fd, "ERROR1");
		game = NULL;
		goto error;
	} else
		do_log(cd, "   game %d is '%s'\n", game_id, game->name);


	if (send_str(cd->fd, "OK") < 0) {
		game = NULL;
		goto error;
	}

	if (server_send_player_names(cd, game) < 0) {
		game = NULL;
		goto error;
	}

	if (server_get_first_spot(cd, game) < 0) {
		game = NULL;
		goto error;
	}

error:
	return game;
}

static int server_get_game_id(ConnData *cd)
{
	int game_id;

	if (read_int(cd->fd, &game_id) < 0) {
		return -1;
	}

	return game_id;
}

static int server_get_new_playernum(ConnData *cd)
{
	int mynump;

	if (read_int(cd->fd, &mynump) < 0) {
		return -1;
	}

	if (send_str(cd->fd, "OK") < 0) {
		return -1;
	}

	return mynump;
}

static char *server_get_new_playername(ConnData *cd)
{
	char buf[256];

	if (read_str(cd->fd, buf, 255) < 0) {
		return NULL;
	}

	return strdup(buf);
}	

static int server_wait_for_ping(ConnData *cd)
{
	char buf[256];

	if (read_str(cd->fd, buf, 255) < 0)
		return -1;

	if (strcmp("READY?", buf) != 0) {
		do_log(cd, "protocol error (got %s)\n", buf);
		return -1;
	}
	return 0;
}

static int server_send_waiting_for_players(ConnData *cd, int nb_missing_players)
{
	if (send_int(cd->fd, nb_missing_players) < 0)
		return -1;

	return 0;
}

static int server_send_last_join(ConnData *cd, int player_num, const char *player_name)
{
	const char *name = player_name ? player_name : "";

	if (send_int(cd->fd, player_num) < 0)
		return -1;

	if (send_str(cd->fd, name) < 0)
		return -1;
	
	return 0;
}

static ConnState do_transition(ConnData *cd, ConnState newstate)
{
	if (cd->state != newstate)
		do_log(cd, "%s => %s\n", state_to_str(cd->state),
		       state_to_str(newstate));
	cd->state = newstate;
	return cd->state;
}

static ConnState get_state_from_command(ConnData *cd, const char *command)
{
	/* New connection, asking to create a game */
	if (cd->state == CONN_NEW && !strcmp(command, "NEWGAME"))
		return do_transition(cd, CONN_SETUP_NEW);
	
	/* New connection, asking to list games */
	if (cd->state == CONN_NEW && !strcmp(command, "LISTGAME"))
		return do_transition(cd, CONN_SETUP_LISTGAME);
	
	if (cd->state == CONN_NEW && !strcmp(command, "TALK_MODE"))
		return do_transition(cd, CONN_SETUP_TALK_MODE);
	
	/* Games listed, doing preliminary join (game info, spot reserve) */
	if (cd->state == CONN_SETUP_LISTGAME && !strcmp(command, "PRELJOIN"))
		return do_transition(cd, CONN_SETUP_PRELJOIN);
	
	/* Prel join done, doing new preliminary join on another game */
	if (cd->state == CONN_SETUP_PRELJOIN && !strcmp(command, "PRELJOIN"))
		return do_transition(cd, CONN_SETUP_PRELJOIN);
	
	/* Prel join done, finalising join */
	if (cd->state == CONN_SETUP_PRELJOIN && !strcmp(command, "JOIN"))
		return do_transition(cd, CONN_SETUP_JOIN);
	
	/* New game setup done, now waiting for players */
	if (cd->state == CONN_SETUP_NEW && !strcmp(command, "WAIT_PLAYERS"))
		return do_transition(cd, CONN_WAIT_PLAYERS);

	/* Game join done, now waiting for players */
	if (cd->state == CONN_SETUP_JOIN && !strcmp(command, "WAIT_PLAYERS"))
		return do_transition(cd, CONN_WAIT_PLAYERS);

	/* Wait for players done, still waiting */
	if (cd->state == CONN_WAIT_PLAYERS && !strcmp(command, "WAIT_PLAYERS"))
		return do_transition(cd, CONN_WAIT_PLAYERS);

	/* Wait for players done, now starting the game */
	if (cd->state == CONN_WAIT_PLAYERS && !strcmp(command, "START_PLAY"))
		return do_transition(cd, CONN_START_PLAY);

	/* Game start done, now playing the game */
	if (cd->state == CONN_START_PLAY && !strcmp(command, "PLAY"))
		return do_transition(cd, CONN_PLAY);

	if (cd->state == CONN_SETUP_TALK_MODE && !strcmp(command, "WAIT"))
		return do_transition(cd, CONN_WAIT_MESSAGE);
	
	return do_transition(cd, CONN_KILL);
}

static ConnState get_state_from_play_command(ConnData *cd, const char command)
{
	/* Playing, going to send a move */
	if (cd->state == CONN_PLAY && command == 'p')
		return do_transition(cd, CONN_PLAY_SEND_MOVE);
	
	/* Playing, going to quit */
	if (cd->state == CONN_PLAY && command == 'q')
		return do_transition(cd, CONN_PLAY_SEND_QUIT);
	
	/* Playing, going to send a move */
	if (cd->state == CONN_PLAY && command == 'r')
		return do_transition(cd, CONN_PLAY_RECEIVE);

	return do_transition(cd, CONN_KILL);	
}

static ConnState server_handle_conn_new(ConnData *cd)
{
	char buf[256];

	set_timeouts(cd, 10);

	if (read_str(cd->fd, buf, 255) < 0)
		return CONN_KILL;

	return get_state_from_command(cd, buf);
}

static ConnState server_handle_conn_setup_new(ConnData *cd)
{
	set_timeouts(cd, 500);

	if (server_setup_game(cd, &cd->game) < 0)
		return CONN_KILL;

	cd->am_initiator = 1;
	cd->player_num = 0;
	return get_state_from_command(cd, "WAIT_PLAYERS");
}

static ConnState server_handle_conn_setup_listgame(ConnData *cd)
{
	char buf[256];

	int nump = 0;

	set_timeouts(cd, 500);

	nump = server_get_requested_num_players(cd);
	do_log(cd, " > requested games with %d players\n", nump);

	if (nump < 1 || nump > 4)
		return CONN_KILL;

	if (server_send_games(cd, nump) < 0)
		return CONN_KILL;

	if (read_str(cd->fd, buf, 255) < 0)
		return CONN_KILL;

	return get_state_from_command(cd, buf);
}

static ConnState server_handle_conn_setup_preljoin(ConnData *cd)
{
	/* preliminary join. user clicked on game.
	 * let him fill his name, bump first_avail_spot
	 */
	char buf[256];
	int game_id;
	Game *game;

	game_id = server_get_game_id(cd);
	if (game_id < 0)
		return CONN_KILL;

	do_log(cd, " > pre-reserving game ID %d\n", game_id);

	SDL_LockMutex (gamelist_mutex);
	game = server_do_reserve_game(cd, game_id);
	if (game)
		cd->previously_reserved = game->id;
	SDL_UnlockMutex(gamelist_mutex);

	/* Reservation should have worked */
	if (cd->previously_reserved < 0)
		return CONN_KILL;

	if (read_str(cd->fd, buf, 255) < 0)
		return CONN_KILL;

	return get_state_from_command(cd, buf);
}

static ConnState server_handle_conn_setup_join(ConnData *cd)
{
	/* player joins reserved game. */
	int mynump;

	SDL_LockMutex (gamelist_mutex);
	cd->game = get_game(cd->previously_reserved);

	if (cd->game == NULL) {
		do_log(cd, " error: no pre-reserved game.\n");
		SDL_UnlockMutex (gamelist_mutex);
		return CONN_KILL;
	}
	cd->previously_reserved = -1;

	do_log(cd, " join on pre-reserved game %d (%s)\n",
		cd->game->id, cd->game->name);

	mynump = server_get_new_playernum(cd);

	do_log(cd, "  > player number %d\n", mynump);

	if (mynump < 0) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	cd->game->player_name[mynump] = server_get_new_playername(cd);

	do_log(cd, " > player name '%s'\n", 
		cd->game->player_name[mynump] 
			? cd->game->player_name[mynump] 
			: "NULL");

	if (cd->game->player_name[mynump] == NULL) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	/* All others but me are remote players */
	cd->game->num_remote = cd->game->num_players - 1;
	cd->game->last_joined = mynump;

	SDL_UnlockMutex (gamelist_mutex);
	cd->player_num = mynump;

	/* see if we can start game */
	return get_state_from_command(cd, "WAIT_PLAYERS");
}

static ConnState server_handle_conn_setup_talk_mode(ConnData *cd)
{
	/* player joins reserved game. */
	int32_t mynump, game_id;

	cd->talk_mode = TRUE;

	if (read_int(cd->fd, &game_id) < 0) {
		do_log(cd, " error: couldn't read game id.\n");
		SDL_UnlockMutex (gamelist_mutex);
		return CONN_KILL;
	}
	
	if (read_int(cd->fd, &mynump) < 0) {
		do_log(cd, " error: couldn't read game id.\n");
		SDL_UnlockMutex (gamelist_mutex);
		return CONN_KILL;
	}
	
	SDL_LockMutex (gamelist_mutex);

	cd->game = get_game(game_id);

	if (cd->game == NULL || cd->game->killed) {
		do_log(cd, " error: Invalid game id %d.\n", game_id);
		SDL_UnlockMutex (gamelist_mutex);
		return CONN_KILL;
	}

	SDL_UnlockMutex (gamelist_mutex);

	cd->player_num = mynump;

	send_str(cd->fd, "OK");

	do_log(cd, " entered talk mode for player %d on game %d.\n",
		mynump, game_id);

	return CONN_WAIT_MESSAGE;
}

static ConnState server_handle_conn_wait_message(ConnData *cd)
{
	if (sock_select(cd->fd, 500) == -1) {
		ConnState new_state;
	
		SDL_LockMutex(gamelist_mutex);

		if (cd->game->killed)
			new_state = CONN_KILL;
		else if (cd->game->message_id > cd->game->players_msg_id[cd->player_num])
			new_state = CONN_RECV_MESSAGE;
		else
			new_state = CONN_WAIT_MESSAGE;

		SDL_UnlockMutex(gamelist_mutex);

		return new_state;
	} else
		return CONN_SEND_MESSAGE;
}

static ConnState server_handle_conn_send_message(ConnData *cd)
{
	char buf[256];
	
	if (read_str(cd->fd, buf, 255) < 0)
		return CONN_KILL;

	SDL_LockMutex(gamelist_mutex);
	if (cd->game->killed) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	do_log(cd, " pushing message '%s' from player %d.\n", buf, cd->player_num);
	game_push_message(cd->game, cd->player_num, buf);
	SDL_UnlockMutex(gamelist_mutex);

	return CONN_WAIT_MESSAGE;
}

static ConnState server_handle_conn_recv_message(ConnData *cd)
{
	int msg_id;
	const Message *msg = NULL;

	SDL_LockMutex(gamelist_mutex);

	if (cd->game->killed) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	msg_id = cd->game->players_msg_id[cd->player_num] + 1;

	msg = &cd->game->messages[msg_id];
	if (send_int(cd->fd, msg->player) < 0 ||
	    send_str(cd->fd, msg->message) < 0) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}
	do_log(cd, " received message '%s' from player %d.\n",
		msg->message, msg->player);

	cd->game->players_msg_id[cd->player_num] = msg_id;
	SDL_UnlockMutex(gamelist_mutex);

	return CONN_WAIT_MESSAGE;
}

static ConnState server_handle_conn_wait_players(ConnData *cd)
{
	/* The game is now setup and we just have to wait for all players to
	 * be there. */
	int nb_missing_players;

	SDL_LockMutex(gamelist_mutex);

	if (cd->game->killed) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	nb_missing_players = get_nb_missing_players(cd->game);

	do_log(cd, " waiting for %d players on game %d (%s)...\n",
		nb_missing_players, cd->game->id, cd->game->name);

	if (server_wait_for_ping(cd) < 0) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	if (server_send_waiting_for_players(cd, nb_missing_players) < 0) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	if (server_send_last_join(cd, cd->game->last_joined,
		cd->game->player_name[cd->game->last_joined]) < 0) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	if (cd->game->last_joined != cd->last_sent_join)
		do_log(cd, " < %d new joins (last '%s')\n",
			cd->game->last_joined,
			cd->game->player_name[cd->game->last_joined]);

	cd->last_sent_join = cd->game->last_joined;

	if (nb_missing_players == 0) {
		do_log(cd, " all players arrived on game %d (%s)\n",
			cd->game->id, cd->game->name);

		cd->game->started = 1;

		SDL_UnlockMutex(gamelist_mutex);

		return get_state_from_command(cd, "START_PLAY");
	}

	SDL_UnlockMutex(gamelist_mutex);
	sleep(1);

	return get_state_from_command(cd, "WAIT_PLAYERS");
}

static ConnState server_handle_conn_start_play(ConnData *cd)
{
	int i;

	SDL_LockMutex(gamelist_mutex);
	cd->game->quit_players = 0;
	cd->game->killed = 0;

	for (i = 0; i < cd->game->num_players; i++) {
		if (send_str(cd->fd, cd->game->player_name[i]) < 0) {
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}
	}
	SDL_UnlockMutex(gamelist_mutex);
	return get_state_from_command(cd, "PLAY");
}

static ConnState server_handle_conn_play(ConnData *cd)
{
	char action;
	int r = read_char(cd->fd, &action);
	if (r != sizeof(char))
		return CONN_KILL;

	return get_state_from_play_command(cd, action);
}

static ConnState server_handle_conn_play_receive(ConnData *cd)
{
	int playercolor, x, y;

	if (read_int(cd->fd, &playercolor) < 0)
		return CONN_KILL;

	SDL_LockMutex(gamelist_mutex);

	do_log(cd, " > game %d: player %d waiting for player %d move\n",
		cd->game->id, cd->player_num, playercolor);

	/* As long as no move has been posted to the game since this player's move id,
	 * wait. */
	while (cd->game->move_id <= cd->game->players_move_id[cd->player_num]) {
		do_log(cd, "   game %d: nothing to send to player %d, waiting (id %d/%d)\n", 
			cd->game->id, cd->player_num, cd->game->players_move_id[cd->player_num],
			cd->game->move_id);
		SDL_CondWait(cd->game->cond_move, gamelist_mutex);
		if (cd->game->killed)
			break;
	}

	while (cd->game->players_move_id[cd->player_num] < cd->game->move_id) {
		int move_id = cd->game->players_move_id[cd->player_num] + 1;

		if (!cd->game->killed) {
			x = cd->game->moves[move_id].x;
			y = cd->game->moves[move_id].y;
		} else {
			x = -2;
			y = -2;
		}

		SDL_UnlockMutex(gamelist_mutex);
		if (send_int(cd->fd, x) < 0) {
			SDL_LockMutex(gamelist_mutex);
			SDL_CondBroadcast(cd->game->cond_wait);
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}
		if (send_int(cd->fd, y) < 0) {
			SDL_LockMutex(gamelist_mutex);
			SDL_CondBroadcast(cd->game->cond_wait);
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}
		SDL_LockMutex(gamelist_mutex);

		if (cd->game->killed) {
			SDL_CondBroadcast(cd->game->cond_wait);
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}

		do_log(cd, " < game %d: pushed move ID %d"
			   " (x %d, y %d) from player %d to player %d\n",
			cd->game->id, move_id, x, y, cd->game->moves[move_id].player,
			cd->player_num);

		cd->game->players_move_id[cd->player_num] = move_id;

	}
	SDL_CondBroadcast(cd->game->cond_wait);
	SDL_UnlockMutex(gamelist_mutex);

	return CONN_PLAY;
}

static void kill_game(Game *game)
{
	if (game->killed == 1)
		return;

	game->killed = 1;
	SDL_CondBroadcast(game->cond_wait);
	SDL_CondBroadcast(game->cond_move);
}

static int players_lagging(ConnData *cd)
{
	int i;

	for (i = 0; i < cd->game->num_players; i++) {
		if (cd->game->players_move_id[i] < cd->game->move_id) {
			do_log(cd, "   game %d lagging: player %d is at move %d, while "
			       "game is at %d\n", cd->game->id, i, 
				cd->game->players_move_id[i], cd->game->move_id);
			return 1;
		}
	}
	return 0;
}

static ConnState server_handle_conn_play_send(ConnData *cd, int quit)
{
	int playercolor, x, y;

	SDL_LockMutex(gamelist_mutex);
	while (players_lagging(cd)) {
		/* wait for the previous move to be received by 
		 * all */
		SDL_CondWait(cd->game->cond_wait, gamelist_mutex);
		if (cd->game->killed) {
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}
	}

	if (read_int(cd->fd, &playercolor) < 0) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	if (playercolor != cd->player_num) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	if (!quit) {
		if (read_int(cd->fd, &x) < 0
		 || read_int(cd->fd, &y) < 0) {
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}
	} else {
		x = -2;
		y = -2;
	}

	if (cd->game->killed) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	if (x != -1 && y != -1) {
		game_push_move(cd->game, playercolor, x, y);
	
		do_log(cd, " < game %d: player %d sends move ID %d (x %d, y %d)\n",
			cd->game->id, playercolor, cd->game->move_id, x, y);
	}

	if (quit) {
		SDL_UnlockMutex(gamelist_mutex);
		return CONN_KILL;
	}

	/* wait here that everyone gets the move. 
	 * this avoids deadlocks */
	while (players_lagging(cd)) {
		SDL_CondWait(cd->game->cond_wait, gamelist_mutex);
		if (cd->game->killed) {
			SDL_UnlockMutex(gamelist_mutex);
			return CONN_KILL;
		}
	}

	SDL_UnlockMutex(gamelist_mutex);
	return CONN_PLAY;
}

static ConnState server_handle_conn_kill(ConnData *cd)
{
	do_log(cd, "Connection done\n", cd);

	if (cd->talk_mode)
	{
		do_log(cd, "Shutting down talk connection\n");
		sock_close(cd->fd);

		return CONN_EXIT;
	}

	SDL_LockMutex(gamelist_mutex);

	if (cd->previously_reserved > 0)
	{
		Game *reserved_game = get_game(cd->previously_reserved);
		server_put_first_spot_back(cd, reserved_game);

		/* Don't let clients not joining after a PRELJOIN kill
		 * games */
		if (reserved_game == cd->game)
			cd->game = NULL;
	}

	if (cd->game) {
		cd->game->quit_players++;
		SDL_CondBroadcast(cd->game->cond_quit);

		if (!cd->game->killed) {
			kill_game(cd->game);
		}

		if (cd->game->quit_players == 1) {
			/* It's us who quit the game */
			do_log(cd, "killing game %d (%d players)\n",
				cd->game->id, cd->game->num_players,
				get_nb_missing_players(cd->game));
		} else {
			do_log(cd, "did quit the game.\n");
		}

		if (cd->am_initiator)
		{
			/* Only the initiator (and not its talk socket) finalizes
			 * the game kill, for obvious double-free reasons */
			do_log(cd, "waiting for %d quit acks before finalizing "
				"game %d kill\n",
				cd->game->num_players 
				- get_nb_missing_players(cd->game)
				- cd->game->quit_players,
				cd->game->id);
			while (cd->game->quit_players < cd->game->num_players 
				- get_nb_missing_players(cd->game))
			{
				/* Wait for all others to be gone */
				SDL_CondWait(cd->game->cond_quit, gamelist_mutex);
			}
			do_log(cd, "finalizing game %d kill\n", cd->game->id);

			gamelist = llist_remove(gamelist, cd->game);
			
			if (dump_dir && cd->game->move_id > 1)
				dump_game(cd->game);

			free_game(cd->game);
			cd->game = NULL;
		}
	}
	SDL_UnlockMutex(gamelist_mutex);

	do_log(cd, "Shutting down connection\n");
	sock_close(cd->fd);

	return CONN_EXIT;
}

static ConnState server_handle_state(ConnData *cd)
{
	switch (cd->state)
	{
	case CONN_NEW:
		return server_handle_conn_new(cd);
	case CONN_SETUP_NEW:
		return server_handle_conn_setup_new(cd);
	case CONN_SETUP_LISTGAME:
		return server_handle_conn_setup_listgame(cd);
	case CONN_SETUP_PRELJOIN:
		return server_handle_conn_setup_preljoin(cd);
	case CONN_SETUP_JOIN:
		return server_handle_conn_setup_join(cd);
	case CONN_SETUP_TALK_MODE:
		return server_handle_conn_setup_talk_mode(cd);
	case CONN_WAIT_PLAYERS:
		return server_handle_conn_wait_players(cd);
	case CONN_START_PLAY:
		return server_handle_conn_start_play(cd);
	case CONN_PLAY:
		return server_handle_conn_play(cd);
	case CONN_PLAY_RECEIVE:
		return server_handle_conn_play_receive(cd);
	case CONN_PLAY_SEND_MOVE:
		return server_handle_conn_play_send(cd, 0);
	case CONN_PLAY_SEND_QUIT:
		return server_handle_conn_play_send(cd, 1);
	case CONN_KILL:
		return server_handle_conn_kill(cd);
	case CONN_WAIT_MESSAGE:
		return server_handle_conn_wait_message(cd);
	case CONN_SEND_MESSAGE:
		return server_handle_conn_send_message(cd);
	case CONN_RECV_MESSAGE:
		return server_handle_conn_recv_message(cd);
	default:
		return CONN_EXIT;
	}
}

static int client_conn_handler(void *data)
{
	ConnData *cd = (ConnData *)data;

	do_log(cd, "New connection\n", cd);

	while (cd->state != CONN_EXIT)
		cd->state = server_handle_state(cd);

	return 0;
}

static LList *clean_finished_threads(LList *threads_list)
{
	LList *cur = threads_list;
	
	while (cur)  {
		ConnData *cd = (ConnData *)cur->data;
		if (cd->state == CONN_EXIT) {
			int status;

			SDL_WaitThread(cd->thread, &status);
			do_log(cd, "Thread exited with status %d\n", status);
			threads_list = llist_remove(threads_list, cd);
			free(cd->addr);
			free(cd);
			cur = threads_list;
		} else
			cur = cur->next;
	}
	do_log(NULL, "%d threads running.\n", llist_length(threads_list));
	return threads_list;
}

static int serversock = -1;
SDL_mutex *serversock_mutex = NULL;

static int timeout_sec = 5;
static int do_broadcast = 0;
static int broadcastsock = -1;
static int magic_number = 0;

void server_stop(void)
{
	SDL_LockMutex(serversock_mutex);
	if (serversock < 0) {
		SDL_UnlockMutex(serversock_mutex);
		return;
	}

	do_log(NULL, "Stopping server.\n");

	sock_close(serversock);
	serversock = -1;

	SDL_UnlockMutex(serversock_mutex);

	if (broadcastsock >= 0) {
		sock_close(broadcastsock);
		broadcastsock = -1;
	}
}


static int wait_for_connection(void)
{
	int sock;
	
	SDL_LockMutex(serversock_mutex);
	sock = serversock;
	SDL_UnlockMutex(serversock_mutex);

	if (sock < 0)
		return -1;

	return sock_select(sock, timeout_sec * 1000);
}

void server_set_select_interval(int timeout)
{
	timeout_sec = timeout;
}

void server_set_do_broadcast(int enable, int magic)
{
	do_broadcast = enable;
	magic_number = magic;
}

static void broadcast_setup(int sock)
{
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
			&optOn, sizeof(optOn));
	setsockopt(sock, SOL_SOCKET, SO_BROADCAST,
			&optOn, sizeof(optOn));
}

static void broadcast_once(void)
{
	struct sockaddr_in br_addr;
	int err;
	char bcast[BCAST_LEN];

	if (broadcastsock < 0) {
		broadcastsock = socket(AF_INET, SOCK_DGRAM, 0);
		broadcast_setup(broadcastsock);
	}
	if (broadcastsock < 0) {
                int err = get_net_error();
		do_log(NULL, "Can't broadcast: %s (%d)\n",
			net_err_str(err), err);
		return;
	}

	br_addr.sin_family = AF_INET;
	br_addr.sin_port = htons(BCAST_PORT);
	br_addr.sin_addr.s_addr = INADDR_BROADCAST;
	
	get_bcast_msg(bcast, BCAST_LEN, magic_number);

	err = sendto(broadcastsock, bcast, sizeof(bcast),
		0, (struct sockaddr *)&br_addr,
		sizeof(br_addr));
	if (err < 0) {
		err = get_net_error();
		do_log(NULL, "Can't send broadcast: %s (%d)\n",
			net_err_str(err), err);
	}
}

int server_start(const char *logfile, const char *game_dump_dir)
{
	int clifd;
#ifndef __MINGW32__
	socklen_t clilen;
#else
	int clilen;
#endif
	struct sockaddr_in serv_addr, cli_addr;
	LList *threads_list = NULL;
	
	if (logfile) {
		logfp = fopen(logfile, "wb");
		if (logfp == NULL) {
			printf("can't open %s\n", logfile);
			logfp = stdout;
		}
	} else
		logfp = stdout;

	if (game_dump_dir)
		dump_dir = game_dump_dir;

	serversock = socket(AF_INET, SOCK_STREAM, 0);
	if (serversock < 0) {
		perror("Error on opening socket");
		return -1;
	}
	
	serversock_mutex = SDL_CreateMutex();
	
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(NET_PORT);
	setsockopt(serversock, SOL_SOCKET, SO_REUSEADDR,
			&optOn, sizeof(optOn));
	if (bind(serversock, (struct sockaddr *) &serv_addr,
		sizeof(serv_addr)) < 0) {
		perror("Error on binding");
		return -1;
	}
	
	do_log(NULL, "Listening on %s\n", inet_ntoa(serv_addr.sin_addr));

	listen(serversock, 1000);
	
	gamelist_mutex = SDL_CreateMutex();
	
	clilen = sizeof(cli_addr);
	while (serversock >= 0) {

		ConnData *cd;

		if (do_broadcast)
			broadcast_once();

		if (wait_for_connection() < 0)
			continue;

		SDL_LockMutex(serversock_mutex);
		if (serversock < 0) {
			SDL_UnlockMutex(serversock_mutex);
			break;
		}

		clifd = accept(serversock, (struct sockaddr *)&cli_addr, &clilen);
		clilen = sizeof(cli_addr);	      
		
		SDL_UnlockMutex(serversock_mutex);

		cd = conn_data_init(clifd, inet_ntoa(cli_addr.sin_addr));

		if (cd != NULL) {
			SDL_Thread *th = SDL_CreateThread(client_conn_handler, cd);
			cd->thread = th;
			threads_list = llist_append(threads_list, cd);
		}

		threads_list = clean_finished_threads(threads_list);
	}
	SDL_DestroyMutex(gamelist_mutex);
	SDL_DestroyMutex(serversock_mutex);
	gamelist_mutex = NULL;
	serversock_mutex = NULL;

	do_log(NULL, "Exiting\n");
	return 0;
}
