/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __WIDGET_H__
#define __WIDGET_H__

typedef struct _Widget Widget;

void text_widget_init(Widget *widget, const char *default_text, int max_len,
		      int x, int y, void (*text_validate_callback)
		      (struct _Widget *widget, void *data),
		      void *text_validate_cb_data);
void text_widget_set_editable(Widget *widget, int editable);
void text_widget_set_text(Widget *widget, const char *text);
const char *text_widget_get_text(Widget *widget);

Widget *widget_create(const char *img_src, int x, int y,
		      void (*clicked_callback)(Widget *widget, void *data),
		      void *clicked_cb_data);
void widget_set_image(Widget *widget, const char *img_src);
void widget_set_clicked_callback(Widget *widget,
		      void (*clicked_callback)(Widget *widget, void *data),
		      void *data);
void list_widget_init(Widget *widget, int list_x, int list_y, int list_width,
		      int (*get_strings)(struct _Widget *widget, char ***strings),
		      void (*list_clicked_callback)(struct _Widget *widget, int index,
						    char *string));
void widget_show(Widget *widget);
void widget_enable(Widget *widget, int enable);
void widget_hide(Widget *widget);
void widget_destroy(Widget *widget);
void update_gui(void);
int gui_wait_event(void);
void gui_reload_images(void);

int widget_get_width(Widget *widget);
int widget_get_height(Widget *widget);
int text_widget_get_x_offset(Widget *widget);
int text_widget_get_y_offset(Widget *widget);
#endif
