/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __PAWN_H__
#define __PAWN_H__

typedef enum {
	PAWN_ORANGE = 0,
	PAWN_BLUE,
	PAWN_RED,
	PAWN_GREEN,
	PAWN_NUM_COLORS
} PawnColor;

typedef struct _Pawn Pawn;

#include "tile.h"

struct _Pawn {
	int num;
	PawnColor color;
	int pos_x;
	int pos_y;
	int coord_x;
	int coord_y;
	int eaten;
	
	LList *just_ate_on; /* tiles */

	SDL_Surface *surface;
};

int pawn_get_max(void);
Pawn *pawn_get(int num, PawnColor color, int reinit);
void pawn_draw (Pawn *pawn);
void pawn_show_eaten(void);
void pawn_free_all(void);

Tile *pawn_get_tile(Pawn *pawn);

LList *pawn_get_surroundings(Tile *tile);

void pawn_eat(Pawn *pawn);
void pawn_eat_fast(Pawn *pawn, int fast);
int pawn_try_and_eat(Pawn *start, Pawn *to_eat);
void pawn_remove_just_eaten(Tile *tile);

int pawn_count(PawnColor color);
void pawn_move_to(Pawn *pawn, Tile *tile);

LList *pawn_get_replacement_pending_pawns(PawnColor color);
LList *pawn_get_all(PawnColor color);

#endif
