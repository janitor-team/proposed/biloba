/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __GAME_H__
#define __GAME_H__

#include <SDL.h>
#include <SDL_thread.h>
#include <SDL_mutex.h>

typedef struct _Game Game;
typedef struct _Move Move;
typedef struct _Message Message;

struct _Move
{
    	int32_t player;
        int32_t x;
        int32_t y;
};

struct _Message
{
    	int32_t player;
        char message[256];
};

struct _Game
{
	char *name;
	int32_t id;
	int32_t num_players;
	int32_t num_remote;
	char *player_name[4];
	int32_t player_type[4];
	int32_t first_avail_spot;
	int32_t started;
	Move *moves;
	int32_t move_id;
	int32_t players_move_id[4];
	int32_t quit_players;
	int32_t killed;
	int32_t last_joined;

	Message *messages;
	int32_t message_id;
	int32_t players_msg_id[4];

	SDL_cond *cond_wait;
	SDL_cond *cond_move;
	SDL_cond *cond_quit;
};

void game_push_move(Game *game, int player, int x, int y);
void game_push_message(Game *game, int player, const char *msg);
void free_game(Game *game);
Game *init_game(void);

#endif
