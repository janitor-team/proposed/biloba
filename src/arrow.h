/*
 * Biloba
 * Copyright (C) 2004-2008 Guillaume Demougeot, Colin Leroy
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 */

#ifndef __ARROW_H__
#define __ARROW_H__

#include "player.h"

typedef enum _ArrowType {
	ARROW_UP,
	ARROW_DOWN,
	ARROW_LEFT,
	ARROW_RIGHT,
	ARROW_UP_LEFT,
	ARROW_UP_RIGHT,
	ARROW_DOWN_LEFT,
	ARROW_DOWN_RIGHT,
	NUM_ARROWS
} ArrowType;

typedef struct _Arrow Arrow;

struct _Arrow {
	ArrowType type;
	SDL_Surface *surface;
};

void   arrow_draw(ArrowType type, int x, int y);
void   arrow_draw_all(Player *player);
#endif
